FROM python:3.8-buster

RUN apt-get update && apt-get install -y graphviz

COPY . /usr/src

RUN pip install -r /usr/src/requirements.txt pytest /usr/src/

WORKDIR /usr/src/

CMD ["pytest", "--junitxml=pytest_report.xml"]
