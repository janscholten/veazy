from veazy.cli import _cli
from pathlib import Path

def test_complete_graph():
	args = (
		(), 		# src 
		None, 		# dst 
		0, 			# complexity_offset 
		None, 		# depth 
		None, 		# root_file
		True,		# whole_graph 
		'svg',		# output_type 
		False,		# launch
	)
	return _cli(*args)

def test_auto_depth():
	rf = str(Path('veazy/cli.py').resolve())
	src = str(Path('veazy/').resolve())
	args = (
		(src,),		# src 
		None, 		# dst 
		0, 			# complexity_offset 
		None, 		# depth 
		rf, 		# root_file
		False,		# whole_graph 
		'svg',		# output_type 
		False,		# launch
	)
	return _cli(*args)
